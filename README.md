Prerequisites:
*  Install Python 2.7.10: https://www.python.org/downloads/
*  Install pip: https://pip.pypa.io/en/stable/installing.html
*  Install Requests: pip install requests --proxy="[user:passwd@]proxy.server:port" ( http://docs.python-requests.org/en/latest/user/install/#install)
*  Install BeautifulSoup: pip install beautifulsoup4 --proxy="[user:passwd@]proxy.server:port" (http://www.crummy.com/software/BeautifulSoup/)
*  Install openpyxl: pip install openpyxl --proxy="[user:passwd@]proxy.server:port" (http://openpyxl.readthedocs.org/en/latest/)
*  Install pyinstaller: pip install pyinstaller --proxy="[user:passwd@]proxy.server:port"
*  create private app via sap anywhere and update the settings.cfg for the parameters client_id,client_secret,refresh_token
*  generate exe for tool: pyinstaller.exe --onefile UploadImageForProduct.py
Notes:
*  There are several initial settings needed to be correctly set before running: "OccDomainName", "UserName", "Password", "ImageFileFolder", "ProductCodeImageMapFilePath", "Proxies". You can set them in the section "USER" of the setting file named "settings.cfg" in advance, or through an interactive interface to set/change them.
*  The format of the setting "Proxies": { "http": "http://user:pass@10.10.1.10:3128/", "https": "http://user:pass@10.10.1.10:1080" }
*  The format of the lines in the product-code-image map file: "ProductCode1,ImageFile1,ImageFile2,ImageFile3,....".
*  The tool will import the products line by line according to the product-code-image map file. If the import is aborted last time, for example, network reasons, this tool will resume the import automatically.
