import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import json
import requests
import base64
from requests import packages
packages.urllib3.disable_warnings()
from bs4 import BeautifulSoup
import ConfigParser
from openpyxl import load_workbook

#####################Configration####################################################
#Take these info from App Area in OCC
productCodeGalleryIdMap = {}
#set proxy
proxies={}
####################################################################################
configFileName = 'settings.cfg'
config = ConfigParser.ConfigParser()
config.optionxform = str
config.read(configFileName)
access_token = ''
headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}
#imageFileFolder = 'image/'

session = requests.Session()

def main():
    try:
        global client_id 
        global client_secret 
        global refresh_token 
        global imageFileFolder 
        global productCodeImageMapFilePath 
        global access_token
        global tokenurl
        global apiurl_predix
        global grant_type
        client_id= readConfig('USER', 'client_id')    
        client_secret= readConfig('USER', 'client_secret')       
        refresh_token= readConfig('USER', 'refresh_token')   
        tokenurl = readConfig('USER', 'tokenurl')  
        grant_type = readConfig('USER', 'grant_type')    
        apiurl_predix = readConfig('USER', 'apiurl_predix')   
        imageFileFolder= readConfig('USER', 'ImageFileFolder')
        imageFileFolder = imageFileFolder.replace('\\', '/')
        if imageFileFolder.rfind('/') != len(imageFileFolder) - 1:
            imageFileFolder = imageFileFolder + '/'
        productCodeImageMapFilePath= readConfig('USER', 'ProductCodeImageMapFilePath')
        access_token = getAccessToken()
        print ('Generated access_token: ' +access_token)
        ############ Input: product code #########
        #Import product images
        lastImportSuccess = isLastImportSuccess()
        lastProductCode = getLastProductCode()
        productCodeImageList = getProductImageMap(productCodeImageMapFilePath)
        for productCodeImage in productCodeImageList:
            productCode = productCodeImage[0]
            #print('productCode: ' +productCode) 
            productId= getProductId(productCode)
            fileNameList = []
            for i in range(1, len(productCodeImage)):
                fileNameList.append(productCodeImage[i])
            try:
                updateProductPicture(productId, fileNameList)
                writeConfig('SYSTEM', 'LastProductCode', productCode)
                print('### Successfully import image(s) for product: ' + productCode)
            except Exception as e:
                writeConfig('SYSTEM', 'LastImportSuccess', 'FALSE')
                print('@@@ Exception: \n{}'.format(str(e)))
        #importProductImage()
        #productCode='product000000'
        #########################################
        #productId= getProductId(productCode)
        #print (productCode+'\'s product id is:' + str(productId))
        #createImageForProduct(productId)
        print ('Upload image finish!')
        writeConfig('SYSTEM', 'LastImportSuccess', 'TRUE')

    except Exception as e:
        writeConfig('SYSTEM', 'LastImportSuccess', 'FALSE')
        print('@@@ Exception: \n{}'.format(str(e)))
        sys.exit(1)


def getProductImageMap(fileName):
    ''' line format: ItemCode1,ImageName1,ImageName2,ImageName3,... '''
    productCodeImageList = []
    if fileName.rfind('.csv') == len(fileName) - 4:
        getProductImageMapFromCsv(fileName, productCodeImageList)
    elif isExcel2010File(fileName):
        getProductImageMapFromExcel(fileName, productCodeImageList)
    else:
        raise Exception('Cannot read the file: {}. The format is not supported.'.format(fileName))

    if len(productCodeImageList) == 0:
        raise Exception('No data found. Please check the map file: {}.'.format(fileName))
    return productCodeImageList


def getProductImageMapFromCsv(fileName, list):
    with open(fileName, 'r', encoding='utf-8') as mapFile:
        for line in mapFile:
            values = line.split(',')
            if values[0].strip() == '':
                continue

            if len(values) < 2:
                raise Exception('''@@@ Error: Please check the map file, which is NOT in a correct format \
on this line: {}'''.format(line))

            productCodeImage = []
            for i in range(len(values)):
                productCodeImage.append(values[i].strip())
            list.append(productCodeImage)


def isExcel2010File(fileName):
    return fileName.rfind('.xlsx') == len(fileName) - 5 \
        or fileName.rfind('.xlsm') == len(fileName) - 5 \
        or fileName.rfind('.xltx') == len(fileName) - 5 \
        or fileName.rfind('.xltm') == len(fileName) - 5


def getProductImageMapFromExcel(fileName, list):
    wb = load_workbook(fileName)
    ws = wb.active
    for row in ws.iter_rows():
        if row[0].value is None or row[0].value.strip() == 'Product Code':
            continue

        if len(row) < 2:
            raise Exception('@@@ Error: Please check the map file, which is NOT in a correct format.')

        productCodeImage = []
        for cell in row:
            if cell.value is not None:
                productCodeImage.append(cell.value.strip())
        list.append(productCodeImage)


def getProductImageMap(fileName):
    ''' line format: ItemCode1,ImageName1,ImageName2,ImageName3,... '''
    productCodeImageList = []
    if fileName.rfind('.csv') == len(fileName) - 4:
        getProductImageMapFromCsv(fileName, productCodeImageList)
    elif isExcel2010File(fileName):
        getProductImageMapFromExcel(fileName, productCodeImageList)
    else:
        raise Exception('Cannot read the file: {}. The format is not supported.'.format(fileName))

    if len(productCodeImageList) == 0:
        raise Exception('No data found. Please check the map file: {}.'.format(fileName))
    return productCodeImageList

#Upload images for productId
def updateProductPicture(productId, fileNameList):
    for imageName in fileNameList:
        url = apiurl_predix + 'Products/'+str(productId)+'/Images'
        #imageName="my.jpg"
        imageFile=open(imageFileFolder+imageName, "rb")
        imageBase64Encoded = base64.b64encode(imageFile.read())
        params = {'access_token': access_token}
        jsonData = {'name':imageName, 'mimeType': 'image/png', 'content': imageBase64Encoded}
        r = doPost(url, jsonData, params)
        rslt = json.loads(r.text)
        print(rslt)  

#Upload image
def createImageForProduct(productId):
    url = apiurl_predix + 'Products/'+str(productId)+'/Images'
    imageName="my.jpg"
    imageFile=open(imageFileFolder+imageName, "rb")
    imageBase64Encoded = base64.b64encode(imageFile.read())
    params = {'access_token': access_token}
    jsonData = {'name':imageName, 'mimeType': 'image/png', 'content': imageBase64Encoded}
    r = doPost(url, jsonData, params)
    rslt = json.loads(r.text)
    return rslt

#Access Token
def getAccessToken():
    params = {'client_id': client_id, 'client_secret': client_secret, 'grant_type': grant_type,
              'refresh_token': refresh_token}

    r = doGet(tokenurl,params)
    result = json.loads(r.text)
    return result['access_token']

#Product id is needed to upload images
def getProductId(productCode):
    url = apiurl_predix + 'Products'
    select='id'
    filter= 'code eq '+ '\'' + productCode + '\''
    params= {'access_token': access_token, 'filter': filter, 'select': select}
    r= doGet(url,params)
    rslt= json.loads(r.text)

    if(len(rslt)>0):
        return rslt[0]['id']
    else:
        return None

def doGet(url, params=None):
    r = session.get(url, headers=headers, proxies=proxies, params=params, verify=False)
    if str(r).find('[200]') < 0:
        raise Exception('Cannot access url: ' + url)
    return r

def doPost(url, json=None, params=None):
    r = session.post(url, headers=headers, json=json, proxies=proxies, params=params, verify=False)
    return r

def readConfig(section, configName):
    try:
        return config.get(section, configName).strip()
    except KeyError:
        return '' if configName != 'Proxies' else '{}'

def writeConfig(section, configName, configValue):
    try:
        config.set(section, configName, configValue)
        with open(configFileName, 'w') as configFile:
            config.write(configFile)
    except KeyError:
        pass

def isLastImportSuccess():
    return str(readConfig('SYSTEM', 'LastImportSuccess')).upper() == 'TRUE'


def getLastProductCode():
    return str(readConfig('SYSTEM', 'LastProductCode'))

if __name__ == '__main__':
    main()

